# Actividades

Hay distintas actividades desarrolladas.

### Futbol

Acceso: > Administrador > Futbol

Características:

- Posibilidad de agregar [categorías](#categorias).
- Puede ingresar [torneos](#torneos), [fixtures](#fixture) y [partidos](#partidos).
- Contempla los [profesores](#profesores).
- Administra días de [entrenamientos](#entrenamientos).
- Relaciona [noticias](#noticias).
- Relaciona [galeria de imágenes](#galeria).

### Artes marciales

Acceso: > Administrador > Artes marciales

Características:

- Puede ingresar [torneos](#torneos).
- Contempla los [profesores](#profesores).
- Administra días de [entrenamientos](#entrenamientos).
- Relaciona [noticias](#noticias).
- Relaciona [galeria de imágenes](#galeria).

### Danzas

Acceso: > Administrador > Danzas

Características:

- Puede ingresar [torneos](#torneos).
- Contempla los [profesores](#profesores).
- Administra días de [entrenamientos](#entrenamientos).
- Relaciona [noticias](#noticias).
- Relaciona [galeria de imágenes](#galeria).

### Gimnasias

Acceso: > Administrador > Gimnasio

Características:

- Contempla los [profesores](#profesores).
- Administra días de [entrenamientos](#entrenamientos).
- Relaciona [noticias](#noticias).
- Relaciona [galeria de imágenes](#galeria).

### Actividades Sociales

Acceso: > Administrador > Actividades Sociales

Características:

- Contempla los [profesores](#profesores).
- Administra días de [entrenamientos](#entrenamientos).
- Relaciona [noticias](#noticias).
- Relaciona [galeria de imágenes](#galeria).

## Profesores

Acceso: > Administrador > Profesores