# Club

Acceso: > Administrador > Clubes

Las propiedades son:

- **Nombre**: es el nombre completo.
- **Nombre Corto**: es el nombre escrito simplificado, se usa para ubicar en partes donde el nombre completo puede resultar malo para el diseño.
- **Dirección**
- **Localidad**
- **Provincia**
- **Descripción**: es un párrafo que se ubica en la pantalla principal. Intanta dar un mensaje de bienvenida. Es tambíen el texto que se mostrará en los buscadores de internet.
- **Historia**: es la historia del club. Este se mostrará como un popup desde varias ubicaciónes.
- **Correo electrónico**
- **Teléfono**
- **Otra Url Web**
- **Escudo**: formato imagen preferentemente 200x200.
- **Colores**: color primario, secundario y terciario. **Estos colores van a dar estilo al sitio.**

## Clubes Rivales

Acceso: > Administrador > Clubes rivales
