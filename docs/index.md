# Bienvenidos

Estás viendo la documentación para usuarios de clux. Mas info [clux.com.ar](http://clux.com.ar).

## Introducción

Clux es un proyecto de sistema online que está orientado a visualizar lo que sucede en los clubes sociales y deportivos para lograr reunir la mayor cantidad de personas que comparten un espacio como puede ser un barrio o un pueblo.

Para esto el sistema se dedica a 2 grandes partes:

- La parte dedicada a actividades deportivas y sociales.
- Y la parte dedicada a la cultura y la educación.

## Administrador

La primera versión del sistema (y única por el momento) se compone de dos partes: una que ven los visitantes al sitio y otra que ven los administradores del sistema. Lo que ven los visitantes es lo que se sube como información en el administrador. Se necesita unas credenciales para ingresar al administrador. Estas credenciales se dan de alta cuando se contrata el servicio, luego el administrador puede generar nuevas credenciales dentro del sistema, para poder dar permisos a otras personas.

Para ingresar al administrador podemos:

- Ir hasta abajo de toda la página y en el "footer" hay un acceso que dice **Administrador**.
- Escribir la palabra **admin** en la URL asi: *http://tuclub.com.ar/admin*.

## Usuarios y Grupos

El club dispone de características para crear tantos usuarios y grupos de usuarios como quiera. Esta es una buena idea para poder crear grupos de profesores que solo accedan a modificar contenido de sus actividades o por ejemplo permitir a una persona que solo acceda a ingresar imágenes y noticias.

Más [info](guia-usuario/usuarios).